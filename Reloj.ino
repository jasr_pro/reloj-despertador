/*
  JASR-PRO 2019
  rolando@jasr.pro

  Esta aplicacion es una reloj desperador con indicador de temperatura.
  La aplicacion se implementó para la tarjeta Arduino Nano. Los componentes 
  de hardware en los que se basa esta aplicacion son los siguientes:

  - LCD 16x2. PIN RS: D8; PIN Enable: D9; PIN RW: GND; PINES 11-14: D4-D7
  - Boton MODE normalmente en alto: D2
  - Boton SET  normalmente en alto: D3
  - BUZZER pasivo: D10
  - LM35 Configuracion Full Range:  A0
  - Puerto Serial TTL: D0(Rx)/D1(Tx)

  Modo de operacion
  La pantalla mostrara en la 1er linea el dia de la semana, la hora del dia en 
  formato de 12 horas. Solamente se mostrara la hora, los minutos y el uso 
  horario (AM/PM). Tambien se mostrara la temperatura en grados centigrados. 
  En la 2da linea de la pantalla se mostrara la alarma con el mismo formato de 
  la hora y un campo que permite activarla o desactivarla.

  Para ajustar el reloj, la alarma o el dia de la semana, se neceista presionar
  inicialmente el boton MODE. Con esto se entrara en modo de ajuste. Con este 
  mismo boton se seleccionara el campo que  que se quiere ajustar y el campo 
  seleccionado comenzara a parpadear. El botón SET se usa para cambiar el valor 
  del campo seleccionado. Para salir del modo de ajuste, el botón MODE se debe 
  presionar hasta que se haya pasado por cada un de los campos. Si los botones 
  se mantienen presionados, la seleccion del campo o el incremento del campo 
  continuara de manera continua. 
  
  El reloj mostrara tambien un separador entre la hora y los minutos (:). Este 
  separador parpadeara al ritmo de 1 segundo, mientras el reloj se encuentre en 
  modo normal. Cuando el reloj entre en modo de ajuste, este separador dejara 
  de parpadear.  

  Si la alarma esta activada y el reloj esta en modo normal, la hora de la alarma
  se revisara cada minuto, y en caso de coincidir con la hora del dia esta se
  activara y el buzzer comenzara a sonar. La alarma dejara de sonar cuando se 
  presione alguno de los botones o haya transcurrido un minuto desde que comenzo 
  a sonar.

  La temperatura se medira y actualizara en la pantalla una vez por minuto.

*/


#include "TimerOne.h"
#include "lcd16x2.h"
#include "buttonx2Int.h"

#define TEMP_PIN A0
#define TEMP_DIFF 0.9
#define VOLTAGE_BOARD_REF 4.65

#define TIMER_TICK 10000  /* Timer tick 10 ms*/
#define BLINK_RATE 50     /* Periodo de Blink , asociado al timer tick */
#define TIMER_TICK_1S_LIMIT 100  /* 1 segundo, asociado al timer tick */

#define FIELD_MAX_LENGTH 5

/* Definicion de indices en base a la tabla tCampoReloj */
#define TIME_HOUR_IDX      0
#define TIME_COLON_IDX     1
#define TIME_MINUTE_IDX    2
#define TIME_SECOND_IDX    3
#define TIME_MERIDIEM_IDX  4
#define WEEK_DAY_IDX       5
#define ALARM_HOUR_IDX     6
#define ALARM_MINUTE_IDX   8
#define ALARM_MERIDIEM_IDX 9
#define ALARM_STATE_IDX    10 
#define TEMP_IDX           11

#define SECONDS_MAX 59
#define MINUTES_MAX 59
#define HOUR_MAX    12
#define HOUR_MIN    1
#define WEEKDAY_MAX 6

#define BUTTON_MODE    BUTTON1
#define BUTTON_ADJUST  BUTTON2  

#define BUZZER 10

enum eMeridiem{
  AM = 0,
  PM = 1
};



enum eWeekDay{
  LUN = 0,
  MAR = 1,
  MIE = 2,
  JUE = 3,
  VIE = 4,
  SAB = 5,
  DOM = 6,
};

enum eAlmState{
  OFF = 0,
  ON = 1
};
enum eFieldType
{
  HOUR_TIME,
  MINUTE_TIME,
  SECOND_TIME,
  MERIDIEM_TIME,
  MINUTE_ALARM,
  MERIDIEM_ALARM,
  HOUR_ALARM,
  WEEK_DAY,
  COLON,
  ALARM_STATE,
  TEMPERATURE
};

enum eRelojOperMode
{
  NORMAL_MODE,
  SET_MODE,
  ALARM_RING_MODE
};

typedef struct {
  byte line;
  byte row;
  byte numChars;
  byte val;
  bool blinkFlag;
  bool blinkState;
  bool show;
  enum eFieldType fieldType;     
}tCampoReloj;


typedef struct{
  unsigned int timeBeep;
  bool   beepState;  
}tAlarmRingSoundCfg;


/* Cada estado es un tiempo en el que se produce un bip o no. Cada estado tiene una duracion */
tAlarmRingSoundCfg alarmRingSoundCfg[] = { {15, true},   /*on*/
                                           {15, false},  /*off*/
                                           {15, true},   
                                           {15, false},
                                           {30, true},
                                           {50, false}                                          
                                         };


typedef struct{
   byte alarmRingNumStates;
   byte alramRingState;
   unsigned int alarmCounter;
   bool   alarmRingDeactivated;
}tAlarmStatus;

typedef struct{
 enum eRelojOperMode operMode;  /* Modo de operacion del reloj */ 
 bool timerTick10ms;  /* Bandera indicando que el timer expiró */ 
 bool tickReloj;  /* Bandera que sincroniza el conteo del reloj (1 segundo) */
 bool minuteTick; /* Bandera que se setea en actividades relacionadas a 1 minuto */
 unsigned int blinkCounter;
 unsigned int tickRelojCounter;
 byte menuCfgPos;
 tAlarmStatus alarmStatus;
}tStatusReloj;

void Config_timer1(void);
void timerCallBack(void);
void relojButtonProcess(byte button);
void SendLcdClockField(tCampoReloj field);
void Blinkcontrol(void);
void clockTimerProcess(void);
void clockProcess(void);
void process1MinuteTasks(void);
void alarmInit(void);
void alarmRingSoundControl(void);
void alarmProcess(void);
void tempRead(void);

unsigned int butCnt[2] = {0,0};


 
tStatusReloj statusReloj = {NORMAL_MODE, false, false, false, 0, 0, 0, 
                            {sizeof(alarmRingSoundCfg)/sizeof(tAlarmRingSoundCfg), 0, 0, false}};

char emptyField[] = "     ";
tCampoReloj camposReloj[] = {  /*Linea Row  #Chars  Val  Blink  BlinkState  Show  Type */
                            {  LINE1, 3,    2,    12,     false,  false,     true, HOUR_TIME},      /* Hora */
                            {  LINE1, 5,    1,     0,     true,  true,     true, COLON},      /* Colon */
                            {  LINE1, 6,    2,     00,     false,  false,     true, MINUTE_TIME},    /* Min  */
                            {  LINE1, 0,    0,     0,     false,  false,     false, SECOND_TIME},
                            {  LINE1, 9,    2, (byte)AM,  false,  false,    true, MERIDIEM_TIME},      /* Uso Horario */
                            {  LINE1, 0,    2, (byte)LUN, false,  false,    true, WEEK_DAY },        /* Dia de la semana*/
                            {  LINE2, 4,   2,    12,     false,  false,     true, HOUR_ALARM},      /* Hora Alarma*/
                            {  LINE2, 6,   1,     0,     false,  false,     true, COLON},      /* Hora */
                            {  LINE2, 7,   2,     0,     false,  false,     true, MINUTE_ALARM},      /* Hora Alarma*/
                            {  LINE2, 10,   2, (byte)AM,  false,  false,    true, MERIDIEM_ALARM},      /* Uso Horario Arlarma*/                       
                            {  LINE2, 13,    3, (byte)OFF, false,  false,    true, ALARM_STATE},
                            {  LINE1, 12,    3,  25,       false, false,     true, TEMPERATURE }
                          };
 
                          
byte numRelojCampos = sizeof(camposReloj)/sizeof(tCampoReloj);



/* Orden en el que el menú se movera para cambiar configuracion */
byte menuOrderIdx[] = { WEEK_DAY_IDX,
                        TIME_HOUR_IDX,
                        TIME_MINUTE_IDX,
                        TIME_MERIDIEM_IDX,                        
                        ALARM_HOUR_IDX,
                        ALARM_MINUTE_IDX,
                        ALARM_MERIDIEM_IDX,
                        ALARM_STATE_IDX
                       };  
byte numMaxMenuOptions = sizeof(menuOrderIdx);


void setup() {
  // put your setup code here, to run once:

  byte idx;
  char temp[] = {0xDF};
  
  Serial.begin(9600); 
  
  Config_timer1();
  lcd16x2Init();
  alarmInit();  
  /* Limipiar la pantalla despues de 2 segundos */
  delay(2000); 
  buttonInit(&relojButtonProcess);
  ClearScreen();

  tempRead();
  /* Mostrar datos iniciales del reloj */
  for (idx=0; idx<numRelojCampos; idx++)
    SendLcdClockField(camposReloj[idx]);
  printCharsLCD(temp, 1, LINE1, 15);
  printCharsLCD("AL", 2, LINE2, 1); 
   

  
}

void loop() {
  // put your main code here, to run repeatedly:
  /* Esta bandera es encencida por el timer cada tick del timer(10 ms). */
  if (statusReloj.timerTick10ms)
  {
    /* Revisa el estdo de los botones y habilita banderas cunado el boton esta listo para leerse */
    buttonTimerTask();
    Blinkcontrol();
    clockTimerProcess();
    alarmRingSoundControl();
    statusReloj.timerTick10ms = false;
  }

  /* Revisa el estado de las banderas y ejecuta las tareas relacionadas a los botones*/ 
  buttonProcess();
  clockProcess();
  process1MinuteTasks();

}

/*------------------------------------------------------------------------------------ */
/* Controla el funcionamiento de cada boton. */
void relojButtonProcess(byte button)
{
  /* At this point is detected that the button is pressed. Use the button as needed */
  byte idx;
  byte minVal, maxVal;
  idx = buttonIdxTranslate(button);
  butCnt[idx]++;
  Serial.print(idx);
  Serial.print(" - ");
  Serial.print(butCnt[idx]);
  Serial.println();

  /* Processo para entrar/salir del modo de configuracion */
  if (button == BUTTON_MODE)
  {
    if (statusReloj.operMode == NORMAL_MODE)
    {
      /* Entrando a SET MODE */
      statusReloj.operMode = SET_MODE;
      statusReloj.menuCfgPos = 0;
      /* Dejar de parpadear el segundero */
      camposReloj[TIME_COLON_IDX].blinkFlag = false;
      SendLcdClockField(camposReloj[TIME_COLON_IDX]);
      idx = menuOrderIdx[statusReloj.menuCfgPos];
      camposReloj[idx].blinkFlag = true;
      camposReloj[idx].blinkState = false;
    }
    else if (statusReloj.operMode == SET_MODE)
    {
      
      /* Borra el blink del estado anterior*/
      idx = menuOrderIdx[statusReloj.menuCfgPos];
      camposReloj[idx].blinkFlag = false;
      camposReloj[idx].blinkState = false;
      SendLcdClockField(camposReloj[idx]);

      statusReloj.menuCfgPos++;
      
      /* Poniendo en parpadeo el campo seleccionado */
      if (statusReloj.menuCfgPos < numMaxMenuOptions)
      {
        idx = menuOrderIdx[statusReloj.menuCfgPos];
        camposReloj[idx].blinkFlag = true;
        camposReloj[idx].blinkState = false;     
      }else
      {
        /* Regresando a modo normal y poniendo el parpadeo en el segundero */
        statusReloj.operMode = NORMAL_MODE;
        camposReloj[TIME_COLON_IDX].blinkFlag = true;
        camposReloj[TIME_SECOND_IDX].val = 0;  /* Reset de los segundos */
        statusReloj.menuCfgPos = 0;
      }
      
    } /* SET_MODE*/
    else if (statusReloj.operMode == ALARM_RING_MODE) /* Desactivando el ring de la alarma al presionar el boton */
    {
      statusReloj.alarmStatus.alarmRingDeactivated = true;
      statusReloj.operMode = NORMAL_MODE;
    }
  } /* BUTTON_MODE*/

  /* Proceso para ajustar cada uno de los campos */
  if (button == BUTTON_ADJUST)
  {
    if (statusReloj.operMode == SET_MODE)
    {
      idx = menuOrderIdx[statusReloj.menuCfgPos];
      switch (idx)
      {
        case TIME_HOUR_IDX:
        {
          minVal=HOUR_MIN; 
          maxVal=HOUR_MAX;
          break;
        }
        case TIME_MINUTE_IDX:
        {
          minVal = 0;
          maxVal = MINUTES_MAX;
          break;
        }
      
        case TIME_MERIDIEM_IDX:
        {
          minVal = (byte)AM;
          maxVal = (byte)PM;
          break;
        }
        case WEEK_DAY_IDX:
        {
          minVal = (byte)LUN;
          maxVal = (byte)DOM;
          break;
        }
        case ALARM_HOUR_IDX:
        {
          minVal = HOUR_MIN;
          maxVal = HOUR_MAX;
          break;
        }
        case ALARM_MINUTE_IDX:
        {
          minVal = 0;
          maxVal = MINUTES_MAX;
          break;
        }
        case ALARM_MERIDIEM_IDX:
        {
          minVal = (byte)AM;
          maxVal = (byte)PM;
          break;
        }
        case ALARM_STATE_IDX:
        {
          minVal = (byte)OFF;
          maxVal = (byte)ON;
          break; 
        }
        default:
        {
          minVal = 0;
          maxVal = 0;
          break;
        }
      }
      /* Con el valor maximo y minimo, incrementar el valor o hacer overflow */
      if (camposReloj[idx].val == maxVal)
      {
        camposReloj[idx].val = minVal;
      }
      else
      {
        camposReloj[idx].val++;
      }

      /* Reseta el contador de parpadeo mientras el boton esta presionado para mostrar de manera correcta los numeros*/
      statusReloj.blinkCounter = 0; 
      camposReloj[idx].blinkFlag = false;  /* Desactiva el parpadeo por un momento para mostrar el nuevo valor en la pantalla */
      SendLcdClockField(camposReloj[idx]); /* Envia el nuevo valor a la pantalla */
      camposReloj[idx].blinkFlag = true; 
      statusReloj.alarmStatus.alarmRingDeactivated = false; /* Asegurandonos de que el chequeo de alarma quede activado */
      
    } /* SET_MODE */
    else if (statusReloj.operMode == ALARM_RING_MODE) /* Desactivando el ring de la alarma al presionar el boton */
    {
      statusReloj.alarmStatus.alarmRingDeactivated = true;
      statusReloj.operMode = NORMAL_MODE;
    }
  }  /* BUTTON_ADJUST */
}
/*----------------------Funciones de Reloj -----------------------------*/
void SendLcdClockField(tCampoReloj field)
{
  
  char strField[FIELD_MAX_LENGTH];

  if (field.show)
  {
    if ((field.fieldType == MERIDIEM_TIME) ||(field.fieldType == MERIDIEM_ALARM))
    {
      switch ((enum eMeridiem)field.val)
      {
        case AM: {sprintf(strField, "%s", "AM"); break;}
        case PM: {sprintf(strField, "%s", "PM"); break;}
        default: sprintf(strField, "%s", "AM");
      }
    }
    else if (field.fieldType == WEEK_DAY) 
    {
      switch ((enum eWeekDay)field.val)
      {
        case LUN: {sprintf(strField, "%s", "Lu"); break;}
        case MAR: {sprintf(strField, "%s", "Ma"); break;}
        case MIE: {sprintf(strField, "%s", "Mi"); break;}
        case JUE: {sprintf(strField, "%s", "Ju"); break;}
        case VIE: {sprintf(strField, "%s", "Vi"); break;}
        case SAB: {sprintf(strField, "%s", "Sa"); break;}
        case DOM: {sprintf(strField, "%s", "Do"); break;}
        default: sprintf(strField, "%s", "Lu");
      }
    }
    else if (field.fieldType == ALARM_STATE)
    {
      switch ((enum eAlmState)field.val)
      {
        case OFF: {sprintf(strField, "%s", "OFF"); break;}
        case ON:  {sprintf(strField,"%s",  "ON "); break;}
        default: sprintf(strField, "%s", "OFF");
      }
    }
    else if (field.fieldType == COLON)
    {
      sprintf(strField, "%s", ":");
    }
    else if (field.fieldType == TEMPERATURE)
    {
      byte temp;
      if ((field.val & 0x80 ) == 0x00)
      {
        /* Positivo */
        sprintf(strField, "%s", " ");
        sprintf(&strField[1], "%d", field.val);
      }
      else
      {
        /* Negativo */
        temp = (~field.val) + 1; /* Obteniendo el valor positivo */
        sprintf(strField, "%s", "-");
        sprintf(&strField[1], "%d", temp);
      }
    }
    else
    {
      if (field.val < 10)
      {  
         if ((field.fieldType == MINUTE_TIME) || (field.fieldType == MINUTE_ALARM))
           sprintf(strField, "%d", 0);
         else
           sprintf(strField, "%s", " ");
         sprintf(&strField[1], "%d", field.val);
      }else{
         sprintf(strField, "%d", field.val);
      }
    }
    
    printCharsLCD(emptyField, field.numChars, field.line, field.row); /* Borra campo inicial */
    if ((!field.blinkFlag) || ((field.blinkFlag) &&(field.blinkState)))
    {
      printCharsLCD(strField, field.numChars, field.line, field.row);   /* Escribe el campo */
    }
  }else{
    printCharsLCD(emptyField, field.numChars, field.line, field.row); /* Borra campo inicial */
  }
  
}

/*-------------------------------------------------------------------------------------------------*/
/* Rutina del parpadeo de los campos del reloj */
void Blinkcontrol(void)
{
  byte idx;

  if (statusReloj.blinkCounter == BLINK_RATE)
  {
    for (idx=0; idx<numRelojCampos; idx++)
    {
      /* Checa en cada campo si la bandera de blink esta encendida. Si lo esta envialo al LCD para imprimir el valor correcto  */
      if (camposReloj[idx].blinkFlag)
      {
        SendLcdClockField(camposReloj[idx]);
        camposReloj[idx].blinkState = !camposReloj[idx].blinkState;
      }      
    }
    statusReloj.blinkCounter = 0;
  }
  else
  {
    statusReloj.blinkCounter++;
  }
}

/* Tarea que fija la base de tiempo para el reloj en base al tick del timer (1 segundo) */
void clockTimerProcess(void)
{
  if (statusReloj.operMode != SET_MODE) /* Deshabilita el conteo de reloj  mientras estamos en SET mode */
  {
    if (statusReloj.tickRelojCounter == TIMER_TICK_1S_LIMIT)
    {
      statusReloj.tickReloj = true;
      statusReloj.tickRelojCounter = 0;              
    }
    else
    {
      statusReloj.tickRelojCounter++;
    }
  }
}

/*  ------------------------------------------------------------------------ */
/* Clock Process, ejecutado cada 1 segundo */
void clockProcess(void)
{

  if (statusReloj.tickReloj)
  {
    /* Actualiza los segundos */
    if (camposReloj[TIME_SECOND_IDX].val == SECONDS_MAX)
    {
      camposReloj[TIME_SECOND_IDX].val = 0;
      /* Tiempo de indicar un nuevo minuto */
      statusReloj.minuteTick = true; 
      /* Actualiza los minutos */
      if (camposReloj[TIME_MINUTE_IDX].val == MINUTES_MAX)
      {
        /* Actualiza las horas */
        if (camposReloj[TIME_HOUR_IDX].val == HOUR_MAX)
        {
          camposReloj[TIME_HOUR_IDX].val = HOUR_MIN;          
        }
        else
        {
          camposReloj[TIME_HOUR_IDX].val++;
          /* Actualiza la zona meridiana */
          if (camposReloj[TIME_HOUR_IDX].val == HOUR_MAX)
          {
            if (((enum eAlmState)(camposReloj[TIME_MERIDIEM_IDX].val)) == AM)
            {
              camposReloj[TIME_MERIDIEM_IDX].val = (byte)(PM);
            }
            else
            {
              camposReloj[TIME_MERIDIEM_IDX].val = (byte)(AM);
              /* Actualizar el dia de la semana */
              switch ((enum eWeekDay)(camposReloj[WEEK_DAY_IDX].val))
              {
                case LUN: {camposReloj[WEEK_DAY_IDX].val = (byte)MAR;  break;}
                case MAR: {camposReloj[WEEK_DAY_IDX].val = (byte)MIE;  break;}
                case MIE: {camposReloj[WEEK_DAY_IDX].val = (byte)JUE;  break;}
                case JUE: {camposReloj[WEEK_DAY_IDX].val = (byte)VIE;  break;}
                case VIE: {camposReloj[WEEK_DAY_IDX].val = (byte)SAB;  break;}
                case SAB: {camposReloj[WEEK_DAY_IDX].val = (byte)DOM;  break;}
                case DOM: {camposReloj[WEEK_DAY_IDX].val = (byte)LUN;  break;}
                default: camposReloj[WEEK_DAY_IDX].val = (byte)MAR;
              }
              SendLcdClockField(camposReloj[WEEK_DAY_IDX]);
            }
            SendLcdClockField(camposReloj[TIME_MERIDIEM_IDX]);
          }
        }
        SendLcdClockField(camposReloj[TIME_HOUR_IDX]);
        camposReloj[TIME_MINUTE_IDX].val = 0;
      }
      else
      {
        camposReloj[TIME_MINUTE_IDX].val++;
      }
      SendLcdClockField(camposReloj[TIME_MINUTE_IDX]);
    }
    else
    {
      camposReloj[TIME_SECOND_IDX].val++;
    }
    
    statusReloj.tickReloj = false;
  }
    
}
/*-----------------------------------------------------------------*/
void alarmInit(void)
{
  pinMode(BUZZER, OUTPUT);
  digitalWrite(BUZZER, 0);
}

/* ----------------------------------------------------------------------------*/
/* Controla el sonido de la Alarma */
void alarmRingSoundControl(void)
{
  byte state;
  unsigned int limit;

   /*  byte alarmRingNumStates; */
   
  
  if ( statusReloj.operMode == ALARM_RING_MODE )
  {  
    state = statusReloj.alarmStatus.alramRingState;
    limit = alarmRingSoundCfg[state].timeBeep;  
    
    if (statusReloj.alarmStatus.alarmCounter == limit)
    {
      statusReloj.alarmStatus.alarmCounter = 0;
      if (state == (statusReloj.alarmStatus.alarmRingNumStates-1))
      {
        statusReloj.alarmStatus.alramRingState = 0;
      }
      else
      {
        statusReloj.alarmStatus.alramRingState++;
      }
      
    }
    else
    {
      digitalWrite(BUZZER, alarmRingSoundCfg[state].beepState);
      statusReloj.alarmStatus.alarmCounter++; 
    }    
  }
  else
  {
    digitalWrite(BUZZER, 0); /* Asegurandose de que la alarma esta apagada */
  }
}

/* --------------------------------------------------------------------------------*/
/* Tarea encargada de revisar la alarma si la alarma debe de activarse */
void alarmProcess(void)
{
 unsigned long int alarmTime, currentTime; 
 if (statusReloj.operMode == NORMAL_MODE)
 {
   if (camposReloj[ALARM_STATE_IDX].val == (byte)ON)
   {
      /* Compare alarm vs time in one single data */ 
      alarmTime = (camposReloj[ALARM_MERIDIEM_IDX].val)<< 16 |
                  (camposReloj[ALARM_HOUR_IDX].val)<< 8  | 
                   camposReloj[ALARM_MINUTE_IDX].val;

      currentTime = (camposReloj[TIME_MERIDIEM_IDX].val)<< 16 |
                  (camposReloj[TIME_HOUR_IDX].val)<< 8  | 
                   camposReloj[TIME_MINUTE_IDX].val;

     /* Activar la alarma si el tiempo coincide y si no se ha desactivado antes */
     if (alarmTime == currentTime)
     { 
        if (statusReloj.alarmStatus.alarmRingDeactivated == false)
        {
          statusReloj.alarmStatus.alramRingState = 0;
          statusReloj.alarmStatus.alarmCounter = 0;
          statusReloj.operMode = ALARM_RING_MODE;       
        }
     }else{
       statusReloj.alarmStatus.alarmRingDeactivated = false;
     }
   }
   else
   {
     statusReloj.alarmStatus.alarmRingDeactivated = false;
   }
 }
 else if (statusReloj.operMode == ALARM_RING_MODE)
 {
    /* La alarma solo sonara 1 minuto si no es desactivada */
    statusReloj.operMode = NORMAL_MODE;
 }
}

/*--------------------------------------------------------------------------------------*/
/* Inicializacion del sensor de temperatura */
void tempRead(void)
{
 
  byte temp;
  // Leer entrada analogica
  int sensorValue = analogRead(TEMP_PIN);
  Serial.println(sensorValue);
  // Convert the analog reading (which goes from 0 - 1023) to a voltage (0 - 5V):
  float tempfloat = sensorValue * (VOLTAGE_BOARD_REF / 1023.0);  
  Serial.println(tempfloat);
  
  tempfloat = (tempfloat - TEMP_DIFF) * 100;  
  Serial.println(tempfloat);
  /* Convertir temperatura en un valor byte */
  if ( tempfloat > 0)
  { 
    temp = (byte)(tempfloat);
  }
  else
  {
    tempfloat *= -1;
    temp = (byte)(tempfloat);
    /*Complemento a 2 */
    temp = (~temp) + 1;
  }
  // print out the value you read:  
  Serial.println(temp);
  camposReloj[TEMP_IDX].val = temp;
  SendLcdClockField(camposReloj[TEMP_IDX]);
}

/*------------  Tareas que se necesitan procesar una vez por minuto --------------*/
void process1MinuteTasks(void)
{
  if ( statusReloj.minuteTick )
  {
    /* Agregar aqui tareas que se deben ejecutar cada segundo como ejemplo la alarma*/
    alarmProcess();
    tempRead();
    statusReloj.minuteTick = false;
  }
}
//--------------------- Timer configuration ----------------------------
void Config_timer1(void){
    Timer1.initialize(TIMER_TICK);
    Timer1.attachInterrupt( timerCallBack, TIMER_TICK);
}

void timerCallBack(void)
{
  statusReloj.timerTick10ms = true;
}
