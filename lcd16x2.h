#ifndef LCD16x2_H
#define LCD16x2_H

#include "Arduino.h"

#define LINE1  1
#define LINE2  2

void lcd16x2Init(void);
void printMsgLCD(char *msg, byte line);
void printCharsLCD(char *msg, byte numChars, byte line, byte row);
void ClearScreen(void);

#endif
