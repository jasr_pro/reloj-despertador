#ifndef BUTTONX2INT_H
#define BUTTONX2INT_H

#include "Arduino.h"

#define NUMBUTTONS 2
#define BUTTON1 2
#define BUTTON2 3
#define DEBOUNCE_TIME  10  /* 100 ms referenciado al tick del timer */
#define PERIODIC_BUTTON_SAMPLE 35 /* 350ms referenciado al tick del timer */  

/* Si se define este modo, se detectara el boton presionado varias veces mientras el 
   boton permanezca presionado. Si no se define solo detectara el boton presionado 
   una vez sin importar el tiempo en que permanezca presionado*/   
#define BUTTON_MODE_CONTINUE    

enum eButtonState
{
  BUTTON_RELEASED,
  BUTTON_PRESSED,
  BUTTON_DEBOUNCE,
};

typedef struct
{
  enum eButtonState buttonState;   /* Estado del boton */
  unsigned int debounceCounter;    /* Contador para el control de rebote */
  bool sampleRdy;                  /* Bandera que indica que el boton fue presionado y esta listo para ejecutar alguna funcion relacionada*/
  void (*buttonRdyFunction)(byte button);  /* Funcion que se ejecuta cuando se detecta que el boton fue presionado */
  #ifdef BUTTON_MODE_CONTINUE
  unsigned int samplePeriodCnt;    /* En el modo continuo de deteccion de presionado del boton, este es el contador que indica cada cuando tomar una muestra*/
  #endif
}tButtonInfo;

/*Inicializacion de la fuancionalidad del control de botones. Se le pasa la funcion que se ejecutara al detectar que el boton fue presionado.
  La funcion que se le pasa recibe como parametro el boton que fue presionado, el cual debe de ser checado y actuar de acuerdo a esto*/
void buttonInit(void (*buttonProcces)(byte) );

/* Funcion que se debe ejecutar en manera periodica a cierto rate(10ms). Se encargara del conteo de tiempos para el control de rebote */
void buttonTimerTask(void);

/* Convierte el numero de boton a un indice que va de 0 a (NUMBUTTONS-1). Este indice es el que se usa internamente para llevar el estatus y control de cada boton */
byte buttonIdxTranslate(byte button);

/* Convierte de numero de indice a numero de boton */
byte getButtonFromIdx(byte idx);

/* Funcion en la que se ejecuta internamente la accion especifica para cada boton. Se debe de ejecutar de manera ciclica. Internamente se checan banderas de estado
   para cada uno de los botones y si se determina que el boton fue presionado (eliminando el rebote) se procede a ejecutar la funcion que se le pasa a este modulo
   cuando se inicializó. */
void buttonProcess(void);
#endif
