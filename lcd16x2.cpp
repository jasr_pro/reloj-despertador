#include "lcd16x2.h"


/* Data: D4 - D7  PD4 - PD7 
   Enable: D9     PB1
   RS    : D8     PB0
*/


void writeLCD4BitData(byte data);
void writeLCDEnable(byte val);
void writeLCDRegSel(byte val);
void writeLCDInstNibble(byte data);
void writeLCDInst(byte data, unsigned int waitTime);
void writeLCDDataNibble(byte data);
void writeLCDData(byte data, unsigned int waitTime);


char line1[] = "  www.jasr.pro  ";
char line2[] = "  Electronica   ";

void lcd16x2Init(void) {
  // put your setup code here, to run once:

  DDRD |= 0xF0; /* Configuring PD4 to PD7 as output */ 
  writeLCD4BitData(0x0); /* Clearing data port*/
  
  DDRB |= 0x3;  /* Configuring PB0 and PB1 as output */ 
  writeLCDRegSel(0);
  writeLCDEnable(0);


  /* At powerup LCD starts in 8 bit mode. It´s required to change it to 4 bit mode 
    http://www.taoli.ece.ufl.edu/teaching/4744/labs/lab7/LCD_V1.pdf 
  */
  delayMicroseconds(15000); /* Delay 15 ms */
  writeLCDInstNibble(0x3);
  delayMicroseconds(4100); /* Delay 4.1 ms */
  writeLCDInstNibble(0x3);
  delayMicroseconds(100); /* Delay 100 us */
  writeLCDInstNibble(0x3);
  delayMicroseconds(4100); /* Delay 4.1 ms */
  writeLCDInstNibble(0x2);

  /* Here we are in Nibble operation. Most significative nibble is sent first*/
  delayMicroseconds(40);
  writeLCDInst(0x28, 40);  /* 4 bit mode, Configuring 2 lines 5x7 character size */  
  writeLCDInst(0x0C, 40);  /* Display On, Cursor Off, Blink Off */
  writeLCDInst(0x01, 1640);  /* Clear Screen & Returns the Cursor Home */
  writeLCDInst(0x06, 40);  /*Inc cursor to the right when writing and don´t shift screen */
  

  byte i;

  printMsgLCD(line1, LINE1);
  printMsgLCD(line2, LINE2);
} 



void writeLCD4BitData(byte data)
{
  byte temp;
  data &= 0xF;  /* Making sure data has only 4 bits */
  temp = PORTD & 0x0F;  /* Read current value of port and clearing bit to overwrite */
  PORTD = temp | ( data << 4);  /* Write back data in port */
}

void writeLCDEnable(byte val)
{
  byte temp;
  val &= 0x1; /* Making sure that data has only 1 bit */
  temp = PORTB & 0xFD;  /* Read current value of port and clearing bit to overwrite */
  PORTB = temp | (val << 1);  /* Write back data in port */
}

void writeLCDRegSel(byte val)
{
  byte temp;
  val &= 0x1; /* Making sure that data has only 1 bit */
  temp = PORTB & 0xFE;  /* Read current value of port and clearing bit to overwrite */
  PORTB = temp | val;  /* Write back data in port */
}


/* Send 1 Nibble to the LCD */
void writeLCDInstNibble(byte data)
{
  writeLCD4BitData(data);
  writeLCDRegSel(0x0);
  writeLCDEnable(0x1);  /* Trigger the write */
  delayMicroseconds(1); /* Wait some time */ 
  writeLCDEnable(0x0);
  delayMicroseconds(3); /* Wait some prior the next instruction */   
}

/* Now writting in */
void writeLCDInst(byte data, unsigned int waitTime)
{ 
  byte nibble;

  /* Send first most significative nibble */
  nibble = data >> 4;
  writeLCDInstNibble(nibble);
  /* Send now  least significative nibble */
  nibble = data & 0xf;
  writeLCDInstNibble(nibble);

  delayMicroseconds(waitTime);  
}

/* Send 1 Nibble to the LCD */
void writeLCDDataNibble(byte data)
{
  writeLCD4BitData(data);
  writeLCDRegSel(0x1);
  writeLCDEnable(0x1);  /* Trigger the write */
  delayMicroseconds(1); /* Wait some time */ 
  writeLCDEnable(0x0);
  delayMicroseconds(1000); /* Wait some prior the next instruction */   
}


void writeLCDData(byte data, unsigned int waitTime)
{ 
  byte nibble;

  /* Send first most significative nibble */
  nibble = data >> 4;
  writeLCDDataNibble(nibble);
  /* Send now  least significative nibble */
  nibble = data & 0xf;
  writeLCDDataNibble(nibble);

  delayMicroseconds(waitTime);  
}

void printMsgLCD(char *msg, byte line)
{
  bool valid = false;
  byte i;
  
  if (line == LINE1) 
  {
    writeLCDInst( 0x80, 41);  /* Row 0 Col 0 */
    valid = true;
  }
  else if (line == LINE2)
  {
    writeLCDInst( 0xC0, 41);  /* Row 1 Col 0 */
    valid = true;
  }

 if (valid)
 {
   for (i=0; i<16; i++)
   {
     if (msg[i]=='\0')
     {
      break;
     }
     /* Sending data directly as ASCII code */
     writeLCDData(msg[i], 41); 
   } 
 }
}


/* Imprime cierto numero de caracteres en la pantalla a partir de la posicion indicada */
void printCharsLCD(char *msg, byte numChars, byte line, byte row)
{

  bool valid = true;
  byte inst, i;

  /* Solo se tienen 2 lineas*/   
  if ((line != LINE1) && (line !=LINE2))
  {
     valid = false;
  }
  
  /* El numero de caracteres debe ser suficiente en la linea */ 
  if ( (row+numChars) > 16)
  {
    valid = false;
  }

  if (valid)
  {
    /* Setear la posicion */
    if (line == LINE1)
    {
      inst= 0x80;
    }       
    if (line == LINE2)
    {
      inst= 0xC0;
    }
    inst |= row;
    writeLCDInst( inst, 41);
    for (i=0; i<numChars; i++)
    { /* Sending data directly as ASCII code */
      writeLCDData(msg[i], 41);   
    }
  }   

}
/* Clear screen */
void ClearScreen(void)
{
  writeLCDInst(0x01, 1640);  /* Clear Screen & Returns the Cursor Home */
  writeLCDInst(0x06, 40);  /*Inc cursor to the right when writing and don´t shift screen */
}
