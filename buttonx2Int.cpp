#include "buttonx2Int.h"

void ISRbutton1();
void ISRbutton2();
void ISRbutton(byte button);

tButtonInfo butInfo[NUMBUTTONS]; /* Informacion del estado de los botones*/


/*Inicializacion de la fuancionalidad del control de botones. Se le pasa la funcion que se ejecutara al detectar que el boton fue presionado.*/
void buttonInit(void (*buttonProcces)( byte button) )
{
  byte idx;
  
  pinMode(BUTTON1, INPUT);
  pinMode(BUTTON2, INPUT);
  
  attachInterrupt(digitalPinToInterrupt(BUTTON1),ISRbutton1, CHANGE);
  attachInterrupt(digitalPinToInterrupt(BUTTON2),ISRbutton2, CHANGE);

  /* Inicializando la informacion de los botones */
  for (idx=0; idx<NUMBUTTONS;idx++)
  {
     butInfo[idx].buttonState = BUTTON_RELEASED;
     butInfo[idx].sampleRdy = false;
     butInfo[idx].buttonRdyFunction = buttonProcces;
  }
}


void ISRbutton1()
{
  ISRbutton(BUTTON1);
}

void ISRbutton2()
{
  ISRbutton(BUTTON2);
}

void ISRbutton(byte button)
{ 
  byte idx;
  idx = buttonIdxTranslate(button);
  butInfo[idx].buttonState = BUTTON_DEBOUNCE;
  butInfo[idx].debounceCounter = 0;
  detachInterrupt(digitalPinToInterrupt(button)); /* Deshabilitando la interrupcion por un rato */
}

/* Convierte de un numero de botton a un indice */
byte buttonIdxTranslate(byte button)
{
  byte idx;
  switch (button)
  {
    case BUTTON1:
     {
       idx = 0;
       break;   
     }
     case BUTTON2:
     {
       idx = 1;
       break;
     }
     default:
       idx = 0;
  }
  return idx;
}

/* Convierte de un indice a un numero de boton */
byte getButtonFromIdx(byte idx)
{
  byte button;
  switch (idx)
  {
    case 0:
     {
       button = BUTTON1;
       break;   
     }
     case 1:
     {
       button = BUTTON2;
       break;
     }
     default:
       button = BUTTON1;
  }
  return button;  
}

/* Esta tarea se espera que se ejecute en cada tick del timer de 10 ms. Maneja el coneto del tiempo de rebote del swicth o debounce */ 
void buttonTimerTask(void)
{
   byte idx, button;
   for (idx=0; idx<NUMBUTTONS; idx++)
   {
      button = getButtonFromIdx(idx);
      if (butInfo[idx].buttonState == BUTTON_DEBOUNCE)
      {
        if ( butInfo[idx].debounceCounter == DEBOUNCE_TIME)
        {
          
          if (digitalRead(button) == 1)
          {
            butInfo[idx].buttonState = BUTTON_PRESSED;
            butInfo[idx].sampleRdy = true;
            #ifdef BUTTON_MODE_CONTINUE
            butInfo[idx].samplePeriodCnt = 0;
            #endif
          }
          else  
          {
            butInfo[idx].buttonState = BUTTON_RELEASED;
            butInfo[idx].sampleRdy = false;
          } 
            
          /* Habilitar interrupcion */
          if (button == BUTTON1)
            attachInterrupt(digitalPinToInterrupt(button),ISRbutton1, CHANGE);
          if (button == BUTTON2)
            attachInterrupt(digitalPinToInterrupt(button),ISRbutton2, CHANGE);
        }
        else
        {
          butInfo[idx].debounceCounter++;
        }
      }

      /* Procesa muestreo perdiodico si el boton se encuentra presionado */
      #ifdef BUTTON_MODE_CONTINUE
      if (butInfo[idx].buttonState == BUTTON_PRESSED)
      {
        if (butInfo[idx].samplePeriodCnt == PERIODIC_BUTTON_SAMPLE)
        {
          butInfo[idx].samplePeriodCnt = 0;
          butInfo[idx].sampleRdy = true;
        }
        else
        {
          butInfo[idx].samplePeriodCnt++;
        }
        
      }
      #endif
   }
}


/* Terea que se debe ejeutar de manera continua. En esta se checa que la bandera de Sample Ready este activa y ejecute la funcion que se 
provee cuando se inicializa la funcion del boton */
void buttonProcess(void)
{

 byte idx, button;
 
 for (idx = 0; idx<NUMBUTTONS; idx++)
 {
   if (butInfo[idx].sampleRdy)
   {
       button = getButtonFromIdx(idx);
       butInfo[idx].buttonRdyFunction(button);
       butInfo[idx].sampleRdy = false; /* Limpa la bandera */
   }
 }

  
}
